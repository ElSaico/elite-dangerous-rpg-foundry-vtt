import fs from "fs";
import path from "path";
import copy from "rollup-plugin-copy";
import scss from "rollup-plugin-scss";
import jscc from "rollup-plugin-jscc";
import foundryPath from "./foundry-path.mjs";

const manifest = JSON.parse(fs.readFileSync("./system.json"));
const systemPath = foundryPath(manifest.id);

console.log(`Bundling to ${systemPath}`);
export default {
  input: [`${manifest.id}.js`],
  output: {
    file: path.join(systemPath, `${manifest.id}.js`),
  },
  watch: {
    clearScreen: true,
  },
  plugins: [
    jscc({
      values: { _ENV: process.env.NODE_ENV },
    }),
    scss({
      output: "./static/edrpg.css",
      failOnError: true,
      watch: "static/scss"
    }),
    copy({
      targets: [
        { src: "./template.json", dest: systemPath },
        { src: "./system.json", dest: systemPath },
        { src: "./static/*", dest: systemPath },
      ],
    }),
  ],
  onwarn(warning, warn) {
    if (warning.code !== "EVAL") {
      warn(warning);
    }
  },
};
