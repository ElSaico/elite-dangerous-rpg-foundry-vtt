export default class EDRPGUtils {
  static findActorById(id) {
    return game.actors.find((actor) => actor.id === id);
  }

  static async findItemByInternalID(internalId, type = null) {
    /** Search local items first */
    let item = game.items.find(
      (item) =>
        item.system.internalId &&
        item.system.internalId.value &&
        item.system.internalId.value.toLowerCase() ===
          internalId.toLowerCase() &&
        (type === null || (item && item.type === type)),
    );
    if (item) {
      return item;
    }
    /** search in compendium */
    const packs = Array.from(game.packs.keys());
    for (const pack of packs) {
      if (pack.startsWith("edrpg")) {
        let packItems = await game.packs.get(pack).getDocuments();
        let item = packItems.find((itemDocument) => {
          const item = itemDocument.toObject();
          return (
            item.system.internalId &&
            item.system.internalId.value &&
            item.system.internalId.value.toLowerCase() ===
              internalId.toLowerCase() &&
            (type === null || (item && item.type === type))
          );
        });
        if (item) {
          return item;
        }
      }
    }
    if (type === null) {
      type = game.i18n.localize("ITEM.Any");
    }
    ui.notifications.warn(
      game.i18n.format("WARN.ItemNotFound", { internalId, type }),
    );
    /** item not found */
    return null;
  }

  static async findItemsByType(type) {
    /** Search local items first */
    let items = {};
    game.items.forEach((item) => {
      if (item.type && item.type === type) {
        items[item.system.internalId.value] = duplicate(item);
      }
    });
    /** search in compendium */
    const packs = Array.from(game.packs.keys());
    for (const pack of packs) {
      if (pack.startsWith("edrpg")) {
        let packItems = await game.packs.get(pack).getDocuments();
        packItems.forEach((item) => {
          if (item.type === type) {
            items[item.system.internalId.value] = duplicate(item);
          }
        });
      }
    }
    return items;
  }

  static isEmpty(value) {
    return !value || value === "" || value === 0 || value === "0";
  }
}
