# Elite Dangerous RPG for Foundry VTT

This is unofficial E.D.R.P.G.   

## Prerequisites
- Minimum Node.js version: 16.16
- Ruby Sass 3.7.4 (or other compatible scss)
- Fondry VTT v10 or v11

## Getting started

1. Run `npm install`
2. Create `foundryconfig.json` file in the main directory of the project and copy the content from the `foundryconfig-example.json` into it.
3. Change the path in `foundryconfig.json` to your Foundry VTT `data` folder
4. To run the project in development environment run `npm start`. To build a release of the project run `npm run build`.

## How it works

### Character Sheet





